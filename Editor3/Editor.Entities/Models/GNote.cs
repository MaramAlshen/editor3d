﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Editor.Entities.Models
{
   public class GNote
    {
        [Key]
        public int GNoteId { get; set; }
        public string Action { get; set; }

        public string Anatomic { get; set; }
        public string Vendor { get; set; }

        public DateTime Time { get; set; }
        //public string Description { get; set; }
    }
}
