﻿using Editor.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Editor.Data.Interfaces
{
    public interface IScene
    {
        void AddScene(Scene scene);
        IEnumerable<Scene> GetLastScene();


    }
}
