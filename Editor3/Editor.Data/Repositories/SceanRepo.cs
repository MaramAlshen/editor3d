﻿using Editor.Data.Context;
using Editor.Data.Interfaces;
using Editor.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Editor.Data.Repositories
{
    public class SceanRepo : IScene
    {
        private readonly AppDbContext _appDbContext;


        //Constaracter
        public SceanRepo(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public void AddScene(Scene scene)
        {
            _appDbContext.Scenes.Add(scene);
            _appDbContext.SaveChanges();
        }

        //...............................................
        //Get Last Scene 
        public IEnumerable<Scene> GetLastScene()
        {
            return _appDbContext.Scenes;
        }
    }
}
